package com.cjx.bean;

import org.springframework.stereotype.Component;

@Component("employee")
public class Employee {

    private Integer empId;
    private String empName;
    private String gender;
    private String email;

    public Employee() {
    }

    public Employee(Integer empId, String empName, String gender, String email) {
        this.empId = empId;
        this.empName = empName;
        this.gender = gender;
        this.email = email;
    }

    public Employee(String empName, String gender, String email) {
        this.empName = empName;
        this.gender = gender;
        this.email = email;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
