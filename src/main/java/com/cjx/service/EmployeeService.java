package com.cjx.service;

import com.cjx.bean.Employee;
import com.cjx.dao.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    public Employee selectEmployeeById(Integer id) {
        return employeeMapper.selectEmployeeById(id);
    }

    public Integer insertEmployee(Employee employee) {
        return employeeMapper.insertEmployee(employee);
    }

    public Integer deleteEmployeeById(Integer id) {
        return employeeMapper.deleteEmployeeById(id);
    }

    public Integer updateEmployee(Employee employee) {

        return employeeMapper.updateEmployee(employee);
    }
}
