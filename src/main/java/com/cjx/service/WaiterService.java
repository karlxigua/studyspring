package com.cjx.service;

public interface WaiterService {

    public void greetTo(String name);
    public void serverTo(String name);

}
