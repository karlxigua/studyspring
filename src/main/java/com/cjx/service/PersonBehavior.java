package com.cjx.service;

public interface PersonBehavior {

    public void work();
    public void play();
}
