package com.cjx.service.impl;

import com.cjx.service.SayHelloService;

public class SayHelloInChinese implements SayHelloService {

    @Override
    public String sayHello() {
        return "你好";
    }
}
