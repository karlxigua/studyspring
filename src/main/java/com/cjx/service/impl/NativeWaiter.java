package com.cjx.service.impl;

import com.cjx.service.WaiterService;
import org.springframework.stereotype.Component;

@Component("nativewaiter")
public class NativeWaiter implements WaiterService {
    @Override
    public void greetTo(String name) {
        System.out.println("greet to " + name + " ...");
    }

    @Override
    public void serverTo(String name) {
        System.out.println("server to " + name + " ...");
    }
}
