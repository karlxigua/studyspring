package com.cjx.service.impl;

import com.cjx.service.PersonBehavior;
import org.springframework.stereotype.Component;

@Component("personbehaviorimpl")
public class PersonBehaviorImpl implements PersonBehavior {
    @Override
    public void work() {
        System.out.println("people work");
    }

    @Override
    public void play() {
        System.out.println("people work");
    }
}
