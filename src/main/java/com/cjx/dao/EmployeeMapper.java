package com.cjx.dao;

import com.cjx.bean.Employee;
import org.springframework.transaction.annotation.Transactional;


public interface EmployeeMapper {

    Employee selectEmployeeById(Integer id);

    Integer insertEmployee(Employee employee);

    Integer deleteEmployeeById(Integer id);

    Integer updateEmployee(Employee employee);
}
