package com.cjx.advice;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 服务生环绕增强类
 */
@Component("greetAround")
public class GreetInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        //获取目标方法入参
        Object[] args = methodInvocation.getArguments();
        //获取方法名称
        String clickName = (String)args[0];
        System.out.println("greetInterceptor: How are you!");
        //利用反射机制调用目标方法
        Object object = methodInvocation.proceed();
        System.out.println("greetInterceptor: Please enjoy yourself!");
        return object;
    }
}
