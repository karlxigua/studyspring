package com.cjx.test;

import com.cjx.pojo.Cat;
import com.cjx.pojo.Person;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class TestSayHello {
    
    public static void main(String[] args) {

//        Resource resource = new FileSystemResource("applicationContext.xml");
//        BeanFactory factory = new XmlBeanFactory(resource);
//        Person p = (Person)factory.getBean("Person");
//        System.out.println(p.sayHello());
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Person person = (Person)context.getBean("Person");
        System.out.println(person.sayHello());
        System.out.println(person.getName() + ", " + person.getAge());

//        Resource resource = new ClassPathResource("applicationContext.xml");
//        BeanFactory factory = new XmlBeanFactory(resource);
//        Person p1 = (Person)factory.getBean("Person");
//        System.out.println(p1.sayHello());

//        List<String> cars = p1.getCars();
//        System.out.println(cars.get(0));
//
//        Map<String, String> kvs = p1.getKvs();
//        System.out.println(kvs.get("k1"));
//
//        Properties properties = p1.getProperties();
//        System.out.println(properties.get("p01"));

        Cat cat = (Cat)context.getBean("cat");


    }
}
