package com.cjx.test;

import com.cjx.bean.Employee;
import com.cjx.dao.EmployeeMapper;
import com.cjx.service.EmployeeService;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class TestMybatis {
//
//    @Autowired
//    EmployeeMapper employeeMapper;
//
//    @Autowired
//    SqlSession sqlSession;
//
//    @Test
//    public void testMybatis(){
//        System.out.println(employeeMapper);
//        EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
//        Employee employee = mapper.selectEmployeeById(1);
//        System.out.println(employee.getEmpName());
//    }




    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        EmployeeService employeeService = (EmployeeService)context.getBean("employeeService");
        System.out.println(employeeService);
        Employee employee = employeeService.selectEmployeeById(2);
        System.out.println(employee.getEmpName());

//        Employee e = new Employee("王五", "男", "333@qq.com");
//        Integer result = employeeService.insertEmployee(e);
//        Integer result = employeeService.deleteEmployeeById(1);
//        System.out.println(result);
        Employee e = new Employee();
        e.setEmail("666@qq.com");
        e.setEmpId(2);
        Integer result = employeeService.updateEmployee(e);
        System.out.println(result);
    }
}
