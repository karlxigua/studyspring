package com.cjx.test;

import com.cjx.pojo.Cat;
import com.cjx.pojo.Person;
import com.cjx.service.PersonBehavior;
import com.cjx.service.impl.PersonBehaviorImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAspect {
    
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        PersonBehaviorImpl personBehavior = (PersonBehaviorImpl) context.getBean("personbehaviorimpl", PersonBehaviorImpl.class);
        personBehavior.play();

        Cat cat = (Cat)context.getBean("cat");
        cat.play();

        Person person = (Person)context.getBean("person");
        System.out.println(person);
    }
}
