package com.cjx.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Component;

@Component("loggingaspectxml")
public class LoggingAspectXml {

    public void loggerBeforeAction(JoinPoint jp) {
        System.out.println(jp.getSignature().getName() + "执行开始");
    }

    public void loggerAroundAction(ProceedingJoinPoint pjp) throws Throwable{
        pjp.proceed();
        System.out.println(pjp.getSignature().getName() + "执行过程中");
    }

    public void loggerAfterAction(JoinPoint jp) {
        System.out.println(jp.getSignature().getName() + "执行结束");
    }
}
