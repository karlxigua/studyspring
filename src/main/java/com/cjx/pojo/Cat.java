package com.cjx.pojo;

import org.springframework.stereotype.Component;

@Component("cat")
public class Cat {

    public Cat() {

    }

    public void play() {
        System.out.println("cat play");
    }
}
