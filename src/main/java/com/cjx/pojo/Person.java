package com.cjx.pojo;

import com.cjx.service.SayHelloService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Properties;

@Component("person")
public class Person {

    private String name;
    private Integer age;
    private SayHelloService sayHello;
    private List<String> cars;
    private Map<String, String> kvs;
    private Properties properties;

    public Person() {
    }

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public SayHelloService getSayHello() {
        return sayHello;
    }

    public void setSayHello(SayHelloService sayHello) {
        this.sayHello = sayHello;
    }

    public String sayHello() {
        return this.sayHello.sayHello();
    }

    public List<String> getCars() {
        return cars;
    }

    public void setCars(List<String> cars) {
        this.cars = cars;
    }

    public Map<String, String> getKvs() {
        return kvs;
    }

    public void setKvs(Map<String, String> kvs) {
        this.kvs = kvs;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
